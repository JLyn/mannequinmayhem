#ifndef CS488_PRIMITIVE_HPP
#define CS488_PRIMITIVE_HPP

#include "algebra.hpp"

class Primitive {
public:

  Primitive();
  virtual ~Primitive();
  virtual void walk_gl(bool assignToOrig) = 0;

  //Can be multiple types of objects
  void *primitive;
};

class Sphere : public Primitive {
public:
  virtual ~Sphere();
  virtual void walk_gl(bool assignToOrig);
};

class Cube : public Primitive {
public:
  virtual ~Cube();
  virtual void walk_gl(bool assignToOrig);
};

#endif
