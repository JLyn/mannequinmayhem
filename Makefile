SOURCES = $(wildcard *.cpp)
INCLUDES = \
    -I. \
    -I/usr/include/SDL
OBJECTS = $(SOURCES:.cpp=.o)
DEPENDS = $(SOURCES:.cpp=.d)
LDFLAGS = $(INCLUDES) $(shell pkg-config --libs lua5.1) -llua5.1 -lpng -lGL -lGLU -lglut -lSDL -lSDL_mixer
CPPFLAGS = $(INCLUDES) $(shell pkg-config --cflags lua5.1) -lGL -lGLU -lglut
CXXFLAGS = $(CPPFLAGS) -W -Wall -g
CXX = g++
MAIN = Mannequin

all: $(MAIN)

depend: $(DEPENDS)

clean:
	rm -f *.o *.d $(MAIN)

$(MAIN): $(OBJECTS)
	@echo Creating $@...
	@$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

%.o: %.cpp
	@echo Compiling $<...
	@$(CXX) -o $@ -c $(CXXFLAGS) $<

%.d: %.cpp
	@echo Building $@...
	@set -e; $(CC) -M $(CPPFLAGS) $< \
                  | sed 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@; \
                [ -s $@ ] || rm -f $@

include $(DEPENDS)
