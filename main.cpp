#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <sys/time.h>

#include "SoundManager.h"
#include "SDL.h"

#ifdef WIN32
#include <windows.h>
#endif /* WIN32 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#ifdef WIN32
#include "glui.h"
#else
//#include <GL/glui.h>
#endif /* WIN32 */

#include "scene_lua.hpp"
#include "Puppet.hpp"
#include "Game.hpp"
#include "Particle.hpp"
#include "LTree.hpp"

//-------------------------------------------------------------------
// defines
//-------------------------------------------------------------------
#define CALLBACK_QUIT 27
#define HITBOXES '+'

Game *g_game = NULL;
long long int g_timer = 0;
bool g_hitboxes = false;
bool g_shadow = false;
Particle g_particles[PARTICLE_SIZE];
LTree *lTrees[3];
char *lStrings[3];
// Particle Texture
GLuint texture[1];

// Create ground texture
#define imageWidth 128
#define imageHeight 128
static GLubyte groundImage[imageHeight][imageWidth][4];
static GLuint texName;
//-------------------------------------------------------------------
// GLUT data
//-------------------------------------------------------------------
int scrWidth, scrHeight;
float cur_x = -1;
float cur_y = 0;
int buttons[5] = {GLUT_UP, GLUT_UP, GLUT_UP, GLUT_UP, GLUT_UP};

int SND_ID_1 , SND_ID_2, SND_ID_3, MUS_ID_4;

float xpos = 0, ypos = 0;

int music_on = 0;
//--------------------------------------------------------------------
//  State variables
//--------------------------------------------------------------------

void makeGroundImage()
{
  int i, j;
  double r, g, b;

  r = 0.5;
  g = 0.3;
  b = 0.1;

  for (i = 0; i < imageHeight; i++) {
    for (j = 0; j < imageWidth; j++) {
       double rr = ((rand() % 500) / 1000.0 + 1.0) * r;
       double gg = ((rand() % 500) / 1000.0 + 1.0) * g;
       double bb = ((rand() % 500) / 1000.0 + 1.0) * b;
       groundImage[i][j][0] = (GLubyte) ((int)255 * rr);
       groundImage[i][j][1] = (GLubyte) ((int)255 * gg);
       groundImage[i][j][2] = (GLubyte) ((int)255 * bb);
       groundImage[i][j][3] = (GLubyte) 255;
    }
  }
}

//-------------------------------------
// fix lights
//-------------------------------------

void lights(){

    GLfloat light_position1[] = {15, 15, 0, 1};
    GLfloat light1[] = {0.5, 0.5, 0.5, 1};
    GLfloat light2[] = {0.5, 0.5, .5, 1.0};
    GLfloat zero[] = {0, 0, 0 , 0};

    // setup
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 25);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light2);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light1);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light2);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position1);


}

Matrix4x4 getShadowMatrix(float plane[4], float light[4])
{
    float  dot;
    double  shadowMat[16];

    dot = plane[0] * light[0] +
          plane[1] * light[1] +
          plane[2] * light[2] +
          plane[3] * light[3];

    shadowMat[0] = dot - light[0] * plane[0];
    shadowMat[4] = -(light[0] * plane[1]);
    shadowMat[8] = -(light[0] * plane[2]);
    shadowMat[12] = -(light[0] * plane[3]);

    shadowMat[1] = -(light[1] * plane[0]);
    shadowMat[5] = dot - light[1] * plane[1];
    shadowMat[9] = -(light[1] * plane[2]);
    shadowMat[13] = -(light[1] * plane[3]);

    shadowMat[2] = -(light[2] * plane[0]);
    shadowMat[6] = -(light[2] * plane[1]);
    shadowMat[10] = dot - light[2] * plane[2];
    shadowMat[14] = -(light[2] * plane[3]);

    shadowMat[3] = -(light[3] * plane[0]);
    shadowMat[7] = -(light[3] * plane[1]);
    shadowMat[11] = -(light[3] * plane[2]);
    shadowMat[15] = dot - light[3] * plane[3];

    return Matrix4x4(shadowMat);
}

//-------------------------------------------------------------------
// Custom render function
//-------------------------------------------------------------------

void render(){

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  glLoadIdentity(); // Reset the view

  // move the scene
  glTranslatef(xpos, ypos, -20);
  //glRotatef(-45.0, 0.0f, 1.0f, 0.0f);

  // Draw textured ground
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glBindTexture(GL_TEXTURE_2D, texName);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0, 0.0); glVertex3f(-25.0, -3.61, 20.0);
  glTexCoord2f(0.0, 1.0); glVertex3f(-25.0, -3.61, -20.0);
  glTexCoord2f(1.0, 1.0); glVertex3f(25.0, -3.61, -20.0);
  glTexCoord2f(1.0, 0.0); glVertex3f(25.0, -3.61, 20.0);
  glEnd();
  glDisable(GL_TEXTURE_2D);

  // Draw puppets
  g_shadow = false;
  g_game->m_puppets[0]->m_root->walk_gl(false);
  g_game->m_puppets[1]->m_root->walk_gl(false);

  if (g_hitboxes)
  {
    //Draw hitboxes
    glColor3d(1.0, 1.0, 0.0);
    glBegin(GL_QUADS);
    glVertex3d(g_game->m_puppets[0]->m_hurtbox[0][0], g_game->m_puppets[0]->m_hurtbox[0][1], g_game->m_puppets[0]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[0]->m_hurtbox[1][0], g_game->m_puppets[0]->m_hurtbox[0][1], g_game->m_puppets[0]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[0]->m_hurtbox[1][0], g_game->m_puppets[0]->m_hurtbox[1][1], g_game->m_puppets[0]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[0]->m_hurtbox[0][0], g_game->m_puppets[0]->m_hurtbox[1][1], g_game->m_puppets[0]->m_hurtbox[0][2]);
    glEnd();
    glBegin(GL_QUADS);
    glVertex3d(g_game->m_puppets[1]->m_hurtbox[0][0], g_game->m_puppets[1]->m_hurtbox[0][1], g_game->m_puppets[1]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[1]->m_hurtbox[1][0], g_game->m_puppets[1]->m_hurtbox[0][1], g_game->m_puppets[1]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[1]->m_hurtbox[1][0], g_game->m_puppets[1]->m_hurtbox[1][1], g_game->m_puppets[1]->m_hurtbox[0][2]);
    glVertex3d(g_game->m_puppets[1]->m_hurtbox[0][0], g_game->m_puppets[1]->m_hurtbox[1][1], g_game->m_puppets[1]->m_hurtbox[0][2]);
    glEnd();
  }

  // L-system trees
  int i, j, matrices = 0;
  glPushMatrix();
  glEnable(GL_COLOR_MATERIAL);
  glTranslatef(8.0, -4.0, -20.0);
  for (i = 0; i < 3; i++)
  {
    // Move to next tree
    j = 0;
    matrices = 0;
    glTranslatef(-5.0, 0.0, 0.0);

    // Draw trunk
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glColor4f(0.5f, 0.25f, 0.05f, 1.0f);
    glBegin(GL_QUADS);
    glVertex3d(0.1, 2.0, 0.0);
    glVertex3d(-0.1, 2.0, 0.0);
    glVertex3d(-0.1, 0.0, 0.0);
    glVertex3d(0.1, 0.0, 0.0);
    glEnd();

    // Branches at top of trunk
    glPushMatrix();
    glTranslatef(0.0, 1.8, 0.0);
    while (lStrings[i][j] != NULL)
    {
      switch(lStrings[i][j])
      {
        case 'A':
          //Do Nothing
          break;
        case 'B':
          if (matrices > 0)
          {
            glPopMatrix();
            matrices--;
          }
          glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
          glColor4f(0.1f, 0.6f, 0.05f, 1.0f);
          glRotated(((matrices % 3 == 2) ? -30.0 : 30.0), 0.0, 0.0, -1.0);
          glBegin(GL_QUADS);
          glVertex3d(0.05, 0.4, 0.0);
          glVertex3d(-0.05, 0.4, 0.0);
          glVertex3d(-0.05, 0.0, 0.0);
          glVertex3d(0.05, 0.0, 0.0);
          glEnd();
          break;
        case 'C':
          glRotated(-35.0, 0.0, 0.0, -1.0);
          glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
          glColor4f(0.5f, 0.25f, 0.05f, 1.0f);
          glBegin(GL_QUADS);
          glVertex3d(0.05, 1.0, 0.0);
          glVertex3d(-0.05, 1.0, 0.0);
          glVertex3d(-0.05, 0.0, 0.0);
          glVertex3d(0.05, 0.0, 0.0);
          glEnd();
          break;
        case 'D':
          glPushMatrix();
          matrices++;
          glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
          glColor4f(0.5f, 0.25f, 0.05f, 1.0f);
          glBegin(GL_QUADS);
          glVertex3d(-0.05, -0.25, 0.0);
          glVertex3d(0.05, -0.25, 0.0);
          glVertex3d(0.05, 0.0, 0.0);
          glVertex3d(-0.05, 0.0, 0.0);
          glEnd();
          glTranslated(0.0, 0.25, 0.0);
          break;
        default:
          break;
      }
      j++;
    }
    while (matrices >= 0)
    {
      glPopMatrix();
      matrices--;
    }
  }
  glPopMatrix();
  //particles
  for (i = 0; i < PARTICLE_SIZE; i++)
  {
    if (g_particles[i].alive)
    {
      float x = g_particles[i].position[0];
      float y = g_particles[i].position[1];
      float z = g_particles[i].position[2];

      glColor3d(g_particles[i].colour.R(), g_particles[i].colour.G(), g_particles[i].colour.B());
      glBegin(GL_TRIANGLE_STRIP);
      glTexCoord2d(1,1); glVertex3f(x+0.02f,y+0.02f,z); // Top Right
      glTexCoord2d(0,1); glVertex3f(x-0.02f,y+0.02f,z); // Top Left
      glTexCoord2d(1,0); glVertex3f(x+0.02f,y-0.02f,z); // Bottom Right
      glTexCoord2d(0,0); glVertex3f(x-0.02f,y-0.02f,z); // Bottom Left
      glEnd();

      g_particles[i].position = g_particles[i].position + g_particles[i].velocity;
      g_particles[i].lifespan--;
      if (g_particles[i].lifespan <= 0)
      {
        g_particles[i].alive = false;
      }
    }
  }

  //ground shadows
  float ground[4] = {0.0f, 1.0f, 0.0f, 3.4f};
  float light[4] = {15.0f, 15.0f, 0.0f, 1.0f};
  Matrix4x4 shadowMat = getShadowMatrix(ground, light);
  glPushMatrix();
  glMultMatrixd(shadowMat.begin());
  g_shadow = true;
  g_game->m_puppets[0]->m_root->walk_gl(false);
  g_game->m_puppets[1]->m_root->walk_gl(false);
  glPopMatrix();

}


//-------------------------------------------------------------------
// motion
//-------------------------------------------------------------------
void motion(int x, int y){
}

//-------------------------------------------------------------------
// mouse
//-------------------------------------------------------------------
void mouse(int button, int state, int x, int y)
{

}

//-------------------------------------------------------------------
// display
//-------------------------------------------------------------------
void display(void)
{

	/* set up for perspective drawing
     */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, scrWidth, scrHeight);
	gluPerspective(40.0, (GLfloat)scrWidth/(GLfloat)scrHeight, 0.1, 1000.0);

	/* change to model view for drawing
     */
	glMatrixMode(GL_MODELVIEW);

	/* Reset modelview matrix
     */
	glLoadIdentity();

	/* Clear framebuffer (both colour and depth buffers)
     */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Set up lighting
  glEnable(GL_NORMALIZE);
  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);
  float lightCoords[4] = {3.0, 3.0, 6.0, 0.0};
  glLightfv(GL_LIGHT0, GL_POSITION, lightCoords);
  glEnable(GL_LIGHT0);

	glEnable(GL_DEPTH_TEST);


	render();

	// dump the whole buffer onto the screen should fix my bug
	glFinish();
	glutSwapBuffers();

}

//-------------------------------------------------------------------
// keyboard
//-------------------------------------------------------------------
void keyboard(unsigned char k, int x, int y)
{
  if (g_game->m_puppets[0]->m_state == Puppet::PS_STANDING ||
      g_game->m_puppets[0]->m_state == Puppet::PS_FORWARD ||
      g_game->m_puppets[0]->m_state == Puppet::PS_BACKWARD)
  {
    switch(k)
    {
      case 'a':
        g_game->m_puppets[0]->setState(Puppet::PS_BACKWARD);
      break;
      case 's':
        g_game->m_puppets[0]->setState(Puppet::PS_STANDING);
      break;
      case 'd':
        g_game->m_puppets[0]->setState(Puppet::PS_FORWARD);
      break;
      case 'q':
        g_game->m_puppets[0]->setState(Puppet::PS_PUNCH);
      break;
      case 'e':
        g_game->m_puppets[0]->setState(Puppet::PS_KICK);
      break;
      default:
      break;
    }
  }

  if (g_game->m_puppets[1]->m_state == Puppet::PS_STANDING ||
      g_game->m_puppets[1]->m_state == Puppet::PS_FORWARD ||
      g_game->m_puppets[1]->m_state == Puppet::PS_BACKWARD)
  {
    switch(k)
    {
      case 'j':
        g_game->m_puppets[1]->setState(Puppet::PS_FORWARD);
      break;
      case 'k':
        g_game->m_puppets[1]->setState(Puppet::PS_STANDING);
      break;
      case 'l':
        g_game->m_puppets[1]->setState(Puppet::PS_BACKWARD);
      break;
      case 'u':
        g_game->m_puppets[1]->setState(Puppet::PS_PUNCH);
      break;
      case 'o':
        g_game->m_puppets[1]->setState(Puppet::PS_KICK);
      break;
      default:
      break;
    }
  }

  switch(k)
  {
    case HITBOXES:
      g_hitboxes = !g_hitboxes;
      break;
    case CALLBACK_QUIT:
      exit(0);
      break;

    default:
      //std::cout << "Key: " << int(k) << std::endl;
      break;
  }


  glutPostRedisplay();
}

//-------------------------------------------------------------------
// init
//-------------------------------------------------------------------
void init(int argc, char** argv)
{

  glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_COLOR_MATERIAL);

	glClearColor(0.00f, 0.80f, 0.80f, 0.0f);

	glEnable(GL_DEPTH_TEST);

	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	lights();

  makeGroundImage();
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  glGenTextures(1, &texName);
  glBindTexture(GL_TEXTURE_2D, texName);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                 GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                 GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth,
              imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
              groundImage);
}

//-------------------------------------------------------------------
// reshape
//-------------------------------------------------------------------
void reshape(int w, int h)
{
  scrWidth = w;
  scrHeight = h;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, w, h);
  gluPerspective(40.0, (GLfloat)w/(GLfloat)h, 0.1, 1000.0);

  /* Reset to modelview matrix mode to avoid confusion later
   */
  glMatrixMode(GL_MODELVIEW);
  glutPostRedisplay();

}

void tick()
{
  struct timeval  tv;
  gettimeofday(&tv, NULL);
  long long int curTime = (long long int)((tv.tv_sec) * 1000 + (tv.tv_usec) / 1000);

  // 20 FPS limit
  if (abs(curTime - g_timer) > 50)
  {
    //update timer
    g_timer = curTime;

    g_game->tick();
    glutPostRedisplay();
  }
}

//-------------------------------------------------------------------
// main
//-------------------------------------------------------------------
int main(int argc, char** argv){

    srand(time(NULL));
    int main_window;
    scrWidth = 1000;
    scrHeight = 700;


    SND_ID_1 = SM.LoadSound("card.wav");
    SND_ID_2 = SM.LoadSound("OBS.wav");
    SND_ID_3 = SM.LoadSound("ghost.wav");
    MUS_ID_4 = SM.LoadMusic("UNREAL.S3M");

    // Loading LUA resources
    std::string filename1 = "puppet.lua";
    std::string filename2 = "cubex.lua";

    // This is how you might import a scene.
    SceneNode *root = import_lua(filename1);
    if (!root) {
      std::cerr << "Could not open " << filename1 << std::endl;
      return 1;
    }
    SceneNode *root2 = import_lua(filename2);
    if (!root2) {
      std::cerr << "Could not open " << filename2 << std::endl;
      return 1;
    }

    // Set up the game
    g_game = new Game();
    g_game->m_puppets[0] = new Puppet(root, true);
    g_game->m_puppets[0]->m_root->rotate('y', 90);
    g_game->m_puppets[0]->m_root->translate(Vector3D(-4.0, 0.0, 0.0));
    g_game->m_puppets[0]->m_root->walk_gl(true);
    g_game->m_puppets[1] = new Puppet(root2, false);
    g_game->m_puppets[1]->m_root->rotate('y', 270);
    g_game->m_puppets[1]->m_root->translate(Vector3D(4.0, 0.0, 0.0));
    g_game->m_puppets[1]->m_root->walk_gl(true);

    // Parse L-system trees
    lTrees[0] = new LTree("A", 1);
    lTrees[1] = new LTree("AA", 2);
    lTrees[2] = new LTree("ABA", 3);
    for (int i = 0; i < 3; i++)
    {
      lStrings[i] = lTrees[i]->expand(i + 3);
      delete lTrees[i];
    }

    // intialize glut and main window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(scrWidth, scrHeight);
    main_window = glutCreateWindow("Mannequin Mayhem");

    // initialize callback
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutIdleFunc(tick);

    init(argc, argv);

    reshape(scrWidth, scrHeight);


    glutMainLoop();

    return 0;
}

