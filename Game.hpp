#ifndef GAME_HPP
#define GAME_HPP

#include <ctime>
#include "Puppet.hpp"
#include "algebra.hpp"

class Game
{
public:

  enum GameState
  {
    GS_FIGHTING,
    GS_KO
  };

  // Create a new game instance
  Game();
  ~Game();

  // Reset Mannequins to starting position
  void reset();

  // Advance the game by one tick.
  void tick();

  void startParticles(Point3D start);

  Puppet *m_puppets[2];

private:

};

#endif

