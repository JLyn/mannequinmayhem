#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include <cstdio>
#include "algebra.hpp"

#define PARTICLE_SIZE 50

class Particle
{
  public:
    Particle();
    virtual ~Particle();

  Vector3D velocity;
  bool alive;
  int lifespan;
  Colour colour;
  Point3D position;
};

#endif // PARTICLE_HPP
