#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include "Game.hpp"
#include "Particle.hpp"

extern Particle g_particles[PARTICLE_SIZE];

Game::Game()
{

}

void Game::reset()
{
  m_puppets[0]->m_root->reset();
  m_puppets[1]->m_root->reset();
}

Game::~Game()
{
  delete[] m_puppets;
}

void Game::tick()
{
  //animate
  m_puppets[0]->update();
  m_puppets[1]->update();

  switch (m_puppets[0]->m_state)
  {
    // Avoid passing through
    case Puppet::PS_FORWARD:
      if (m_puppets[0]->m_hurtbox[1][0] > m_puppets[1]->m_hurtbox[0][0])
      {
        m_puppets[0]->m_root->translate(Vector3D(-0.1, 0.0, 0.0));
        m_puppets[0]->setState(Puppet::PS_STANDING);
      }
    break;

    case Puppet::PS_PUNCH:
      if (m_puppets[0]->m_timeInState == 5)
      {
        Point3D fist = m_puppets[0]->getFist();
        if (fist[0] >= m_puppets[1]->m_hurtbox[0][0] && fist[0] <= m_puppets[1]->m_hurtbox[1][0] &&
            fist[1] <= m_puppets[1]->m_hurtbox[0][1] && fist[1] >= m_puppets[1]->m_hurtbox[1][1])
        {
          // HIT!
          m_puppets[1]->setState(Puppet::PS_HIT_PUNCH);
          startParticles(fist);
        }
      }
    break;
    case Puppet::PS_KICK:
      if (m_puppets[0]->m_timeInState == 14)
      {
        Point3D foot = m_puppets[0]->getFoot();

        if (foot[0] >= m_puppets[1]->m_hurtbox[0][0] && foot[0] <= m_puppets[1]->m_hurtbox[1][0] &&
            foot[1] <= m_puppets[1]->m_hurtbox[0][1] && foot[1] >= m_puppets[1]->m_hurtbox[1][1])
        {
          // HIT!
          m_puppets[1]->setState(Puppet::PS_HIT_KICK);
          startParticles(foot);
        }
      }
    break;
    default:
    break;
  }

  switch (m_puppets[1]->m_state)
  {
    case Puppet::PS_FORWARD:
      if (m_puppets[1]->m_hurtbox[0][0] < m_puppets[0]->m_hurtbox[1][0])
      {
        m_puppets[1]->m_root->translate(Vector3D(0.1, 0.0, 0.0));
        m_puppets[1]->setState(Puppet::PS_STANDING);
      }
    break;

    case Puppet::PS_PUNCH:
      if (m_puppets[1]->m_timeInState == 5)
      {
        Point3D fist = m_puppets[1]->getFist();
        if (fist[0] >= m_puppets[0]->m_hurtbox[0][0] && fist[0] <= m_puppets[0]->m_hurtbox[1][0] &&
            fist[1] <= m_puppets[0]->m_hurtbox[0][1] && fist[1] >= m_puppets[0]->m_hurtbox[1][1])
        {
          // HIT!
          m_puppets[0]->setState(Puppet::PS_HIT_PUNCH);
          startParticles(fist);
        }
      }
    break;
    case Puppet::PS_KICK:
      if (m_puppets[1]->m_timeInState == 14)
      {
        Point3D foot = m_puppets[1]->getFoot();

        if (foot[0] >= m_puppets[0]->m_hurtbox[0][0] && foot[0] <= m_puppets[0]->m_hurtbox[1][0] &&
            foot[1] <= m_puppets[0]->m_hurtbox[0][1] && foot[1] >= m_puppets[0]->m_hurtbox[1][1])
        {
          // HIT!
          m_puppets[0]->setState(Puppet::PS_HIT_KICK);
          startParticles(foot);
        }
      }
    break;
    default:
    break;
  }
}

void Game::startParticles(Point3D start)
{
  int i;
  srand(time(NULL));
  for (i = 0; i < PARTICLE_SIZE; i++)
  {
    g_particles[i].alive = true;
    g_particles[i].lifespan = 20;
    g_particles[i].colour = Colour(1.0, 0.0, 0.2);
    g_particles[i].position = start;
    g_particles[i].velocity = Vector3D((rand() % 10 - 5) / 100.0f, (rand() % 10 - 5) / 100.0f, (rand() % 10 - 5) / 100.0f);
  }
}
