#include "primitive.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

Primitive::Primitive()
{
  primitive = NULL;
}

Primitive::~Primitive()
{
}

Sphere::~Sphere()
{
    if (primitive != NULL)
      gluDeleteQuadric((GLUquadricObj *)primitive);
}

void Sphere::walk_gl(bool assignToOrig)
{
  if (!assignToOrig)
  {
    GLUquadricObj *sphere;
    sphere = gluNewQuadric();
    gluQuadricDrawStyle(sphere, GLU_FILL);

    gluSphere(sphere, 1.0, 17, 17);

    if (primitive != NULL)
      gluDeleteQuadric((GLUquadricObj *)primitive);
    primitive = (void *)sphere;
  }
}

Cube::~Cube()
{

}

void Cube::walk_gl(bool assignToOrig)
{
  if (!assignToOrig)
  {
    glutSolidCube(1.0);
  }
}
