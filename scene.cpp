#include "scene.hpp"
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>

#define EPSILON 0.000000000000001
int g_nextId = 0;
extern bool g_shadow;

SceneNode::SceneNode(const std::string& name)
  : m_name(name)
{
  m_id = g_nextId;
  g_nextId++;
}

SceneNode::~SceneNode()
{
}

void SceneNode::walk_gl(bool assignToOrig)
{
  glPushMatrix();
  //OpenGL reads in column major form, not row major, so transpose
  Matrix4x4 transposed = m_unscaledTrans.transpose();
  glMultMatrixd(transposed.begin());
  if (assignToOrig)
    assign_to_originals();
  std::list<SceneNode*>::const_iterator it;
  for (it = m_children.begin(); it != m_children.end(); it++)
  {
    (*it)->walk_gl(assignToOrig);
  }

  glPopMatrix();
}

void SceneNode::rotate(char axis, double angle)
{
  double radAngle = degrees2Rad(angle);

  double transformationX[16] = {          1,              0,             0,           0,
                                          0,  cos(radAngle),-sin(radAngle),           0,
                                          0,  sin(radAngle), cos(radAngle),           0,
                                          0,              0,             0,           1};

  double transformationY[16] = { cos(radAngle),           0, sin(radAngle),           0,
                                             0,           1,             0,           0,
                                -sin(radAngle),           0, cos(radAngle),           0,
                                             0,           0,             0,           1};

  double transformationZ[16] = { cos(radAngle), -sin(radAngle),          0,           0,
                                 sin(radAngle),  cos(radAngle),          0,           0,
                                             0,              0,          1,           0,
                                             0,              0,          0,           1};


  switch (axis)
  {
    case 'x':
      m_rotation = Matrix4x4(transformationX) * m_rotation;
      break;

    case 'y':
      m_rotation = Matrix4x4(transformationY) * m_rotation;
      break;

    case 'z':
      m_rotation = Matrix4x4(transformationZ) * m_rotation;
      break;

    default:
      break;
  }

  refresh_matrices();
}

void SceneNode::scale(const Vector3D& amount)
{
  double transformation[16] = {amount[0],           0,          0,           0,
                                       0,   amount[1],          0,           0,
                                       0,           0,  amount[2],           0,
                                       0,           0,          0,           1};

  m_scale = Matrix4x4(transformation) * m_scale;

  refresh_matrices();
}

void SceneNode::translate(const Vector3D& amount)
{
  double transformation[16] = {   1,           0,          0,   amount[0],
                                  0,           1,          0,   amount[1],
                                  0,           0,          1,   amount[2],
                                  0,           0,          0,           1};

  m_translation = Matrix4x4(transformation) * m_translation;

  refresh_matrices();
}

bool SceneNode::is_joint() const
{
  return false;
}

void SceneNode::refresh_matrices()
{
  m_trans = m_translation * m_rotation * m_scale;
  m_unscaledTrans = m_translation * m_rotation;

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
    {
      if (m_trans[i][j] < EPSILON && m_trans[i][j] > -EPSILON)
        m_trans[i][j] = 0.0f;
      if (m_unscaledTrans[i][j] < EPSILON && m_unscaledTrans[i][j] > -EPSILON)
        m_unscaledTrans[i][j] = 0.0f;
    }
  }
}

void SceneNode::assign_to_originals()
{
  m_origTrans = m_trans;
  m_origRotation = m_rotation;
  m_origScale = m_scale;
  m_origTranslation = m_translation;
  m_origUnscaledTrans = m_unscaledTrans;
}

void SceneNode::reset()
{
  m_trans = m_origTrans;
  m_rotation = m_origRotation;
  m_scale = m_origScale;
  m_translation = m_origTranslation;
  m_unscaledTrans = m_origUnscaledTrans;

  std::list<SceneNode*>::const_iterator it;
  for (it = m_children.begin(); it != m_children.end(); it++)
  {
    (*it)->reset();
  }

}

JointNode::JointNode(const std::string& name)
  : SceneNode(name)
{
}

JointNode::~JointNode()
{
}

void JointNode::walk_gl(bool assignToOrig)
{
  SceneNode::walk_gl(assignToOrig);
}

bool JointNode::is_joint() const
{
  return true;
}

void JointNode::set_joint_x(double min, double init, double max)
{
  m_joint_x.min = min;
  m_joint_x.init = init;
  m_joint_x.max = max;
  m_currentX = init;
}

void JointNode::set_joint_y(double min, double init, double max)
{
  m_joint_y.min = min;
  m_joint_y.init = init;
  m_joint_y.max = max;
  m_currentY = init;
}

void JointNode::rotateC(char axis, double angle)
{
  if (axis == 'x')
  {
    if (angle + m_currentX < m_joint_x.min || angle + m_currentX > m_joint_x.max)
      return;
    else
    {
      SceneNode::rotate(axis, angle);
      m_currentX += angle;
    }
  }
  if (axis == 'y')
  {
    if (angle + m_currentY < m_joint_y.min || angle + m_currentY > m_joint_y.max)
      return;
    else
    {
      SceneNode::rotate(axis, angle);
      m_currentY += angle;
    }
  }
}

GeometryNode::GeometryNode(const std::string& name, Primitive* primitive)
  : SceneNode(name),
    m_primitive(primitive)
{
  m_material = NULL;
}

GeometryNode::~GeometryNode()
{
}

void GeometryNode::walk_gl(bool assignToOrig)
{
  glPushMatrix();
  //OpenGL reads in column major form, not row major, so transpose
  if (assignToOrig)
    assign_to_originals();
  else
  {
    if (m_primitive != NULL)
    {
      if (m_material != NULL)
      {
        m_material->apply_gl();
      }
      Matrix4x4 transposed = m_trans.transpose();
      glMultMatrixd(transposed.begin());
      m_primitive->walk_gl(assignToOrig);
    }
    else
    {
      Matrix4x4 transposed = m_unscaledTrans.transpose();
      glMultMatrixd(transposed.begin());
      std::list<SceneNode*>::const_iterator it;
      for (it = m_children.begin(); it != m_children.end(); it++)
      {
        (*it)->walk_gl(assignToOrig);
      }
    }
  }
  glPopMatrix();
}

