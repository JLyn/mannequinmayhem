#ifndef PUPPET_HPP
#define PUPPET_HPP

#include "scene.hpp"
#define NECK 2
#define HEAD 4
#define LSHOULDER 8
#define LELBOW 10
#define LWRIST 12
#define RSHOULDER 14
#define RELBOW 16
#define RWRIST 18
#define LHIP 21
#define LKNEE 23
#define LANKLE 25
#define RHIP 27
#define RKNEE 29
#define RANKLE 31
#define NODES_PER_PUPPET 34

#define MODEL_HURTBOX0   Point3D(0.0, 2.4, 1.2)
#define MODEL_HURTBOX1   Point3D(0.0, -3.4, -0.55)
#define MODEL_HURTBOX2   Point3D(0.0, 2.4, -0.55)
#define MODEL_HURTBOX3   Point3D(0.0, -3.4, 1.2)

class Puppet
{
  public:
    enum PuppetState
    {
      PS_STANDING,
      PS_CROUCHING,
      PS_FORWARD,
      PS_BACKWARD,
      PS_PUNCH,
      PS_KICK,
      PS_SPECIAL,
      PS_CPUNCH,
      PS_HIT_PUNCH,
      PS_HIT_KICK,
      PS_BLOCKING
    };

    Puppet(SceneNode *node, bool facingRight);
    virtual ~Puppet();

    void assign_ids(SceneNode *node);

    void update();
    void setState(PuppetState state);
    void reset();
    void resetWithoutRoot();

    Point3D getFist();
    Point3D getFoot();

    SceneNode *m_root;

    JointNode *m_head;
    JointNode *m_neck;
    JointNode *m_lShoulder;
    JointNode *m_lElbow;
    JointNode *m_lWrist;
    JointNode *m_rShoulder;
    JointNode *m_rElbow;
    JointNode *m_rWrist;
    JointNode *m_lHip;
    JointNode *m_lKnee;
    JointNode *m_lAnkle;
    JointNode *m_rHip;
    JointNode *m_rKnee;
    JointNode *m_rAnkle;

    PuppetState m_state;
    PuppetState m_prevState;
    long long int m_timeInState;
    bool m_facingRight;
    Point3D m_hurtbox[2];
};

#endif // PUPPET_HPP
