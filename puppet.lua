--
-- CS488 -- Introduction to Computer Graphics
--
-- puppet.lua

rootnode = gr.node('root')

red = gr.material({1.0, 0.0, 0.0}, {0.1, 0.1, 0.1}, 10)
blue = gr.material({0.0, 0.0, 1.0}, {0.1, 0.1, 0.1}, 10)
green = gr.material({0.0, 1.0, 0.0}, {0.1, 0.1, 0.1}, 10)
white = gr.material({1.0, 1.0, 1.0}, {0.1, 0.1, 0.1}, 10)

-------------------------------------------------------------
-- MY PUPPET
-------------------------------------------------------------

torso = gr.sphere('torso')
torso:scale(0.6, 1.2, 0.6)
torso:set_material(green)
rootnode:add_child(torso)

collar = gr.joint('collar', {0,0,90}, {0,0,0})
collar:translate(0.0, 1.2, 0.0)
rootnode:add_child(collar)

neck = gr.sphere('neck')
neck:scale(0.15, 0.25, 0.15)
neck:translate(0, 0.2, 0)
neck:set_material(green)
collar:add_child(neck)

jaw = gr.joint('jaw', {0,0,90}, {-90,0,90})
jaw:translate(0.0, 0.3, 0.0)
collar:add_child(jaw)

head = gr.sphere('head')
head:scale(0.55, 0.55, 0.55)
head:translate(0.0, 0.45, 0.0)
head:set_material(green)
jaw:add_child(head)

nose = gr.sphere('nose')
nose:scale(0.1, 0.1, 0.1)
nose:translate(0.0, 0.4, 0.5)
nose:set_material(red)
jaw:add_child(nose)

shoulders = gr.sphere('shoulders')
shoulders:scale(1.0, 0.3, 0.3)
shoulders:translate(0.0, 1.0, 0.0)
shoulders:set_material(green)
rootnode:add_child(shoulders)

lshoulder = gr.joint('lshoulder', {-90,0,0}, {0,0,0})
lshoulder:translate(1.0, 1.0, 0.0)
rootnode:add_child(lshoulder)

luarm = gr.sphere('luarm')
luarm:translate(0.0, -0.5, 0.0)
luarm:scale(0.2, 0.5, 0.2)
luarm:set_material(green)
lshoulder:add_child(luarm)

lelbow = gr.joint('lelbow', {-90,0,0}, {0,0,0})
lelbow:translate(0.0, -1.0, 0.0)
lshoulder:add_child(lelbow)

llarm = gr.sphere('llarm')
llarm:scale(0.15, 0.5, 0.15)
llarm:translate(0.0, -0.5, 0.0)
llarm:set_material(green)
lelbow:add_child(llarm)

lwrist = gr.joint('lwrist', {-90,0,0}, {0,0,0})
lwrist:translate(0.0, -1.0, 0.0)
lelbow:add_child(lwrist)

lhand = gr.sphere('lhand')
lhand:scale(0.15, 0.15, 0.15)
lhand:translate(0.0, -0.15, 0.0)
lhand:set_material(green)
lwrist:add_child(lhand)

rshoulder = gr.joint('rshoulder', {-90,0,0}, {0,0,0})
rshoulder:translate(-1.0, 1.0, 0.0)
rootnode:add_child(rshoulder)

ruarm = gr.sphere('ruarm')
ruarm:translate(0.0, -0.5, 0.0)
ruarm:scale(0.2, 0.5, 0.2)
ruarm:set_material(green)
rshoulder:add_child(ruarm)

relbow = gr.joint('relbow', {-90,0,0}, {0,0,0})
relbow:translate(0.0, -1.0, 0.0)
rshoulder:add_child(relbow)

rlarm = gr.sphere('rlarm')
rlarm:scale(0.15, 0.5, 0.15)
rlarm:translate(0.0, -0.5, 0.0)
rlarm:set_material(green)
relbow:add_child(rlarm)

rwrist = gr.joint('rwrist', {-90,0,0}, {0,0,0})
rwrist:translate(0.0, -1.0, 0.0)
relbow:add_child(rwrist)

rhand = gr.sphere('rhand')
rhand:scale(0.15, 0.15, 0.15)
rhand:translate(0.0, -0.15, 0.0)
rhand:set_material(green)
rwrist:add_child(rhand)

hips = gr.sphere('hips')
hips:scale(0.6, 0.45, 0.45)
hips:translate(0.0, -1.3, 0.0)
hips:set_material(green)
rootnode:add_child(hips)

lhip = gr.joint('lhip', {-90,0,0}, {0,0,0})
lhip:translate(0.5, -1.4, 0.0)
rootnode:add_child(lhip)

lthigh = gr.sphere('lthigh')
lthigh:scale(0.35, 0.6, 0.35)
lthigh:translate(0.0, -0.6, 0.0)
lthigh:set_material(green)
lhip:add_child(lthigh)

lknee = gr.joint('lknee', {0,0,90}, {0,0,0})
lknee:translate(0.0, -1.1, 0.0)
lhip:add_child(lknee)

lcalf = gr.sphere('lcalf')
lcalf:scale(0.3, 0.6, 0.3)
lcalf:translate(0.0, -0.6, 0.0)
lcalf:set_material(green)
lknee:add_child(lcalf)

lankle = gr.joint('lankle', {0,0,90}, {0,0,0})
lankle:translate(0.0, -1.2, 0.0)
lknee:add_child(lankle)

lfoot = gr.sphere('lfoot')
lfoot:scale(0.25, 0.2, 0.35)
lfoot:translate(0.0, 0.0, 0.35)
lfoot:set_material(green)
lankle:add_child(lfoot)

rhip = gr.joint('rhip', {-90,0,0}, {0,0,0})
rhip:translate(-0.5, -1.4, 0.0)
rootnode:add_child(rhip)

rthigh = gr.sphere('rthigh')
rthigh:scale(0.35, 0.6, 0.35)
rthigh:translate(0.0, -0.6, 0.0)
rthigh:set_material(green)
rhip:add_child(rthigh)

rknee = gr.joint('rknee', {0,0,90}, {0,0,0})
rknee:translate(0.0, -1.1, 0.0)
rhip:add_child(rknee)

rcalf = gr.sphere('rcalf')
rcalf:scale(0.3, 0.6, 0.3)
rcalf:translate(0.0, -0.6, 0.0)
rcalf:set_material(green)
rknee:add_child(rcalf)

rankle = gr.joint('rankle', {0,0,90}, {0,0,0})
rankle:translate(0.0, -1.2, 0.0)
rknee:add_child(rankle)

rfoot = gr.sphere('rfoot')
rfoot:scale(0.25, 0.2, 0.35)
rfoot:translate(0.0, 0.0, 0.35)
rfoot:set_material(green)
rankle:add_child(rfoot)

boxy = gr.cube('boxy')
boxy:scale(0.4, 0.4, 0.4)
boxy:translate(0.0, 0.0, 0.75)
boxy:set_material(red)
rootnode:add_child(boxy)

return rootnode
