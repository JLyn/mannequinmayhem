#include "Puppet.hpp"
#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

Puppet::Puppet(SceneNode *node, bool facingRight)
{
  m_root = node;
  if (m_root != NULL)
    assign_ids(m_root);

  m_facingRight = facingRight;

  m_prevState = PS_STANDING;
  m_state = PS_STANDING;
  m_timeInState = 0;
}

Puppet::~Puppet()
{
  delete m_root;
  //dtor
}

void Puppet::assign_ids(SceneNode *node)
{
  std::list<SceneNode*>::iterator it;
  for (it = node->m_children.begin(); it != node->m_children.end(); it++)
  {
    switch((*it)->m_id % NODES_PER_PUPPET)
    {
      case HEAD:
        m_head = dynamic_cast<JointNode *>(*it);
        break;
      case NECK:
        m_neck = dynamic_cast<JointNode *>(*it);
        break;
      case LSHOULDER:
        m_lShoulder = dynamic_cast<JointNode *>(*it);
        break;
      case LELBOW:
        m_lElbow = dynamic_cast<JointNode *>(*it);
        break;
      case LWRIST:
        m_lWrist = dynamic_cast<JointNode *>(*it);
        break;
      case RSHOULDER:
        m_rShoulder = dynamic_cast<JointNode *>(*it);
        break;
      case RELBOW:
        m_rElbow = dynamic_cast<JointNode *>(*it);
        break;
      case RWRIST:
        m_rWrist = dynamic_cast<JointNode *>(*it);
        break;
      case LHIP:
        m_lHip = dynamic_cast<JointNode *>(*it);
        break;
      case LKNEE:
        m_lKnee = dynamic_cast<JointNode *>(*it);
        break;
      case LANKLE:
        m_lAnkle = dynamic_cast<JointNode *>(*it);
        break;
      case RHIP:
        m_rHip = dynamic_cast<JointNode *>(*it);
        break;
      case RKNEE:
        m_rKnee = dynamic_cast<JointNode *>(*it);
        break;
      case RANKLE:
        m_rAnkle = dynamic_cast<JointNode *>(*it);
        break;
      default:
        break;
    }
    assign_ids(*it);
  }
}

void Puppet::update()
{
  //setup hurtboxes
  if (!m_facingRight)
  {
    m_hurtbox[0] = m_root->m_trans * MODEL_HURTBOX0;
    m_hurtbox[1] = m_root->m_trans * MODEL_HURTBOX1;
  }
  else
  {
    m_hurtbox[0] = m_root->m_trans * MODEL_HURTBOX2;
    m_hurtbox[1] = m_root->m_trans * MODEL_HURTBOX3;
  }

//      PS_STANDING,
//      PS_CROUCHING,
//      PS_FORWARD,
//      PS_BACKWARD,
//      PS_PUNCH,
//      PS_SPECIAL,
//      PS_CPUNCH,
//      PS_HIT,
//      PS_BLOCKING,
//      PS_CHIT

  bool done = false;

  //Animation list
  //If 0, initialize the animation
  //Otherwise, some periodic animation is occuring
  switch (m_state)
  {
    case PS_STANDING:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else if (m_timeInState % 20 < 5 || m_timeInState % 20 >= 15)
      {
        m_lShoulder->rotate('x', 3.0);
        m_rShoulder->rotate('x', -3.0);
      }
      else
      {
        m_lShoulder->rotate('x', -3.0);
        m_rShoulder->rotate('x', 3.0);
      }
    break;
    case PS_FORWARD:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else
      {
        if (m_facingRight)
        {
          if (m_hurtbox[1][0] < 8.0)
            m_root->translate(Vector3D(0.1, 0.0, 0.0));
        }
        else
        {
          if (m_hurtbox[1][0] > -8.0)
          m_root->translate(Vector3D(-0.1, 0.0, 0.0));
        }
        if (m_timeInState % 20 < 10)
        {
          m_lHip->rotate('x', -3.0);
          m_rHip->rotate('x', 0.8);
        }
        else
        {
          m_lHip->rotate('x', 3.0);
          m_rHip->rotate('x', -0.8);
        }
      }
    break;
    case PS_BACKWARD:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else
      {
        if (m_facingRight)
        {
          if (m_hurtbox[1][0] > -8.0)
            m_root->translate(Vector3D(-0.1, 0.0, 0.0));
        }
        else
        {
          if (m_hurtbox[1][0] < 8.0)
            m_root->translate(Vector3D(0.1, 0.0, 0.0));
        }
        if (m_timeInState % 20 < 10)
        {
          m_lHip->rotate('x', -3.0);
          m_rHip->rotate('x', 0.8);
        }
        else
        {
          m_lHip->rotate('x', 3.0);
          m_rHip->rotate('x', -0.8);
        }
      }
    break;

    case PS_PUNCH:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else if (m_timeInState < 5)
      {
        m_rShoulder->rotate('x', -12.0);
        m_rElbow->rotate('x', 14.0);
      }
      else if (m_timeInState < 10)
      {
        m_rShoulder->rotate('x', 12.0);
        m_rElbow->rotate('x', -14.0);
      }
      else
      {
        setState(PS_STANDING);
        done = true;
      }
    break;
    case PS_KICK:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else if (m_timeInState < 5)
      {
        m_lHip->rotate('x', -9.0);
        m_lKnee->rotate('x', 9.0);
      }
      else if (m_timeInState < 14)
      {
        m_lKnee->rotate('x', -10.0);
      }
      else if (m_timeInState < 20)
      {
        m_lKnee->rotate('x', 15.0);
      }
      else if (m_timeInState < 23)
      {
        m_lHip->rotate('x', 15.0);
        m_lKnee->rotate('x', -15.0);
      }
      else
      {
        setState(PS_STANDING);
        done = true;
      }
    break;

    case PS_HIT_PUNCH:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else if (m_timeInState < 6)
      {
        m_head->rotate('x', -7.0);
        if (m_facingRight)
        {
          if (m_hurtbox[1][0] > -8.0)
            m_root->translate(Vector3D(-0.3, 0.0, 0.0));
        }
        else
        {
          if (m_hurtbox[1][0] < 8.0)
            m_root->translate(Vector3D(0.3, 0.0, 0.0));
        }
      }
      else
      {
        setState(PS_STANDING);
        done = true;
      }
    break;
    case PS_HIT_KICK:
      if (m_timeInState == 0)
      {
        resetWithoutRoot();
      }
      else if (m_timeInState < 12)
      {
        m_head->rotate('x', 3.0);
        m_lElbow->rotate('x', 3.0);
        m_rElbow->rotate('x', 3.0);
        m_lHip->rotate('x', -0.1);
        m_lKnee->rotate('x', -3.0);
        m_rHip->rotate('x', -0.1);
        m_rKnee->rotate('x', -3.0);

        if (m_facingRight)
        {
          if (m_hurtbox[1][0] > -8.0)
            m_root->translate(Vector3D(-0.3, 0.0, 0.0));
        }
        else
        {
          if (m_hurtbox[1][0] < 8.0)
            m_root->translate(Vector3D(0.3, 0.0, 0.0));
        }
      }
      else
      {
        setState(PS_STANDING);
        done = true;
      }
    break;

    default:
      break;
  }

  if (done == true)
    m_timeInState = 0;
  else
  {
    m_timeInState++;
    if (m_timeInState > 1000000)
      m_timeInState = 1;
  }
}

Point3D Puppet::getFist()
{
  return m_root->m_trans * m_rShoulder->m_trans * m_rElbow->m_trans * m_rWrist->m_trans * Point3D(0.0, 0.0, 0.0);
}

Point3D Puppet::getFoot()
{
  return m_root->m_trans * m_lHip->m_trans * m_lKnee->m_trans * m_lAnkle->m_trans * Point3D(0.0, 0.0, 0.0);
}

void Puppet::setState(PuppetState state)
{
  m_prevState = m_state;
  m_state = state;
  m_timeInState = 0;
}

void Puppet::reset()
{
  m_root->reset();

  m_lShoulder->rotate('x', -45.0);
  m_rShoulder->rotate('x', -45.0);
  m_lElbow->rotate('x', -90.0);
  m_rElbow->rotate('x', -90.0);
  m_lHip->rotate('x', -50.0);
  m_lKnee->rotate('x', 50.0);
  m_rHip->rotate('x', -50.0);
  m_rHip->rotate('y', -90.0);
  m_rKnee->rotate('x', 50.0);
}

void Puppet::resetWithoutRoot()
{
  Matrix4x4 matrices[5];
  matrices[0] = m_root->m_trans;
  matrices[1] = m_root->m_unscaledTrans;
  matrices[2] = m_root->m_scale;
  matrices[3] = m_root->m_rotation;
  matrices[4] = m_root->m_translation;
  reset();
  m_root->m_trans = matrices[0];
  m_root->m_unscaledTrans = matrices[1];
  m_root->m_scale = matrices[2];
  m_root->m_rotation = matrices[3];
  m_root->m_translation = matrices[4];
}
