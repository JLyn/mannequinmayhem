#ifndef SCENE_HPP
#define SCENE_HPP

#include <list>
#include "algebra.hpp"
#include "primitive.hpp"
#include "material.hpp"

class SceneNode {
public:
  SceneNode(const std::string& name);
  virtual ~SceneNode();

  virtual void walk_gl(bool assignToOrig = false);

  const Matrix4x4& get_transform() const { return m_trans; }

  void set_transform(const Matrix4x4& m)
  {
    m_trans = m;
  }

  void set_transform(const Matrix4x4& m, const Matrix4x4& i)
  {
    m_trans = m;
  }

  void add_child(SceneNode* child)
  {
    m_children.push_back(child);
  }

  void remove_child(SceneNode* child)
  {
    m_children.remove(child);
  }

  // Used to compose the transformation and inverse matrices
  // Called inside transformation callbacks
  void refresh_matrices();

  // Declares the current matrices of this and children as original matrices
  void assign_to_originals();
  // Sets the node and all children back to their original values
  void reset();

  // Callbacks to be implemented.
  // These will be called from Lua.
  void rotate(char axis, double angle);
  void scale(const Vector3D& amount);
  void translate(const Vector3D& amount);

  // Returns true if and only if this node is a JointNode
  virtual bool is_joint() const;

  // Hierarchy
  typedef std::list<SceneNode*> ChildList;
  ChildList m_children;

  // Useful for assignToOrig
  std::string m_name;
  int m_id;

  // Transformations
  Matrix4x4 m_trans;
  Matrix4x4 m_rotation;
  Matrix4x4 m_scale;
  Matrix4x4 m_translation;
  Matrix4x4 m_unscaledTrans;

  // Originals from the LUA file.
  Matrix4x4 m_origTrans;
  Matrix4x4 m_origRotation;
  Matrix4x4 m_origScale;
  Matrix4x4 m_origTranslation;
  Matrix4x4 m_origUnscaledTrans;
};

class JointNode : public SceneNode {
public:
  JointNode(const std::string& name);
  virtual ~JointNode();

  virtual void walk_gl(bool bicking = false);
  void rotateC(char axis, double angle);

  virtual bool is_joint() const;

  void set_joint_x(double min, double init, double max);
  void set_joint_y(double min, double init, double max);

  struct JointRange {
    double min, init, max;
  };


protected:

  JointRange m_joint_x, m_joint_y;
  double m_currentX;
  double m_currentY;
};

class GeometryNode : public SceneNode {
public:
  GeometryNode(const std::string& name,
               Primitive* primitive);
  virtual ~GeometryNode();

  virtual void walk_gl(bool assignToOrig = false);

  const Material* get_material() const;
  Material* get_material();

  void set_material(Material* material)
  {
    m_material = material;
  }

protected:
  Material* m_material;
  Primitive* m_primitive;
};

#endif
