#include "Particle.hpp"

Particle::Particle() :
colour(1.0, 0.0, 0.0)
{
  velocity = Vector3D((rand() % 10 - 5) / 100.0f, (rand() % 10 - 5) / 100.0f, (rand() % 10 - 5) / 100.0f);
  alive = false;
  lifespan = 0;
  position = Point3D(0.0, 0.0, 0.0);
}

Particle::~Particle()
{

}
