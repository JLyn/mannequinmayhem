#include "LTree.hpp"
#include <cstdlib>
#include <iostream>

LTree::LTree(char *grammar, int size)
{
  m_grammar.reserve(size);
  char *c = grammar;
  for (int i = 0; i < size; i++)
  {
    if (*c != NULL)
    {
      m_grammar.push_back(*c);
    }
  }

  //Hard-coded rules
  // A > BAB, B > CDC
  m_rules.push_back("ADAB");
  m_rules.push_back("BCB");
}

LTree::~LTree()
{
  //dtor
}

char * LTree::expand(int iterations)
{
  int i;
  int numInserted;
  std::vector<char> grammar = m_grammar;
  std::vector<char> retvec;

  for (i = 0; i < iterations; i++)
  {
    std::vector<char>::iterator it;
    for (it = grammar.begin(); it != grammar.end(); it++)
    {
      std::vector<char *>::iterator ruleIt;
      bool foundRule = false;
      for (ruleIt = m_rules.begin(); ruleIt != m_rules.end(); ruleIt++)
      {
        if ((*ruleIt)[0] == *it)
        {
          numInserted = 0;
          while ((*ruleIt)[numInserted + 1] != NULL)
          {
            retvec.push_back((*ruleIt)[numInserted + 1]);
            numInserted++;
          }
          foundRule = true;
          break;
        }
      }
      if (!foundRule)
      {
        retvec.push_back(*it);
      }
    }
    grammar = retvec;
    retvec.clear();
  }

  char *retval = new char[grammar.size() + 1];
  for (i = 0; i < grammar.size(); i++)
  {
    retval[i] = grammar.at(i);
  }
  retval[grammar.size()] = NULL;
  return retval;
}
