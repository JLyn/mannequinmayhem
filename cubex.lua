--
-- CS488 -- Introduction to Computer Graphics
--
-- puppet.lua

rootnode = gr.node('root')

red = gr.material({1.0, 0.0, 0.0}, {0.1, 0.1, 0.1}, 10)
blue = gr.material({0.0, 0.0, 1.0}, {0.1, 0.1, 0.1}, 10)
green = gr.material({0.0, 1.0, 0.0}, {0.1, 0.1, 0.1}, 10)
white = gr.material({1.0, 1.0, 1.0}, {0.1, 0.1, 0.1}, 10)

-------------------------------------------------------------
-- MY PUPPET
-------------------------------------------------------------

torso = gr.cube('torso')
torso:scale(1.2, 2.4, 1.2)
torso:set_material(blue)
rootnode:add_child(torso)

collar = gr.joint('collar', {0,0,90}, {0,0,0})
collar:translate(0.0, 1.2, 0.0)
rootnode:add_child(collar)

neck = gr.cube('neck')
neck:scale(0.3, 0.5, 0.3)
neck:translate(0, 0.2, 0)
neck:set_material(blue)
collar:add_child(neck)

jaw = gr.joint('jaw', {0,0,90}, {-90,0,90})
jaw:translate(0.0, 0.3, 0.0)
collar:add_child(jaw)

head = gr.cube('head')
head:scale(1.1, 1.1, 1.1)
head:translate(0.0, 0.45, 0.0)
head:set_material(blue)
jaw:add_child(head)

nose = gr.cube('nose')
nose:scale(0.2, 0.2, 0.2)
nose:translate(0.0, 0.4, 0.5)
nose:set_material(red)
jaw:add_child(nose)

shoulders = gr.cube('shoulders')
shoulders:scale(2.0, 0.6, 0.6)
shoulders:translate(0.0, 1.0, 0.0)
shoulders:set_material(blue)
rootnode:add_child(shoulders)

lshoulder = gr.joint('lshoulder', {-90,0,0}, {0,0,0})
lshoulder:translate(1.0, 1.0, 0.0)
rootnode:add_child(lshoulder)

luarm = gr.cube('luarm')
luarm:translate(0.0, -0.5, 0.0)
luarm:scale(0.4, 1.0, 0.4)
luarm:set_material(blue)
lshoulder:add_child(luarm)

lelbow = gr.joint('lelbow', {-90,0,0}, {0,0,0})
lelbow:translate(0.0, -1.0, 0.0)
lshoulder:add_child(lelbow)

llarm = gr.cube('llarm')
llarm:scale(0.3, 1.0, 0.3)
llarm:translate(0.0, -0.5, 0.0)
llarm:set_material(blue)
lelbow:add_child(llarm)

lwrist = gr.joint('lwrist', {-90,0,0}, {0,0,0})
lwrist:translate(0.0, -1.0, 0.0)
lelbow:add_child(lwrist)

lhand = gr.cube('lhand')
lhand:scale(0.3, 0.3, 0.3)
lhand:translate(0.0, -0.15, 0.0)
lhand:set_material(blue)
lwrist:add_child(lhand)

rshoulder = gr.joint('rshoulder', {-90,0,0}, {0,0,0})
rshoulder:translate(-1.0, 1.0, 0.0)
rootnode:add_child(rshoulder)

ruarm = gr.cube('ruarm')
ruarm:translate(0.0, -0.5, 0.0)
ruarm:scale(0.4, 1.0, 0.4)
ruarm:set_material(blue)
rshoulder:add_child(ruarm)

relbow = gr.joint('relbow', {-90,0,0}, {0,0,0})
relbow:translate(0.0, -1.0, 0.0)
rshoulder:add_child(relbow)

rlarm = gr.cube('rlarm')
rlarm:scale(0.3, 1.0, 0.3)
rlarm:translate(0.0, -0.5, 0.0)
rlarm:set_material(blue)
relbow:add_child(rlarm)

rwrist = gr.joint('rwrist', {-90,0,0}, {0,0,0})
rwrist:translate(0.0, -1.0, 0.0)
relbow:add_child(rwrist)

rhand = gr.cube('rhand')
rhand:scale(0.3, 0.3, 0.3)
rhand:translate(0.0, -0.15, 0.0)
rhand:set_material(blue)
rwrist:add_child(rhand)

hips = gr.cube('hips')
hips:scale(1.2, 0.9, 0.9)
hips:translate(0.0, -1.3, 0.0)
hips:set_material(blue)
rootnode:add_child(hips)

lhip = gr.joint('lhip', {-90,0,0}, {0,0,0})
lhip:translate(0.5, -1.4, 0.0)
rootnode:add_child(lhip)

lthigh = gr.cube('lthigh')
lthigh:scale(0.7, 1.2, 0.7)
lthigh:translate(0.0, -0.6, 0.0)
lthigh:set_material(blue)
lhip:add_child(lthigh)

lknee = gr.joint('lknee', {0,0,90}, {0,0,0})
lknee:translate(0.0, -1.1, 0.0)
lhip:add_child(lknee)

lcalf = gr.cube('lcalf')
lcalf:scale(0.6, 1.2, 0.6)
lcalf:translate(0.0, -0.6, 0.0)
lcalf:set_material(blue)
lknee:add_child(lcalf)

lankle = gr.joint('lankle', {0,0,90}, {0,0,0})
lankle:translate(0.0, -1.2, 0.0)
lknee:add_child(lankle)

lfoot = gr.cube('lfoot')
lfoot:scale(0.5, 0.4, 0.7)
lfoot:translate(0.0, 0.0, 0.35)
lfoot:set_material(blue)
lankle:add_child(lfoot)

rhip = gr.joint('rhip', {-90,0,0}, {0,0,0})
rhip:translate(-0.5, -1.4, 0.0)
rootnode:add_child(rhip)

rthigh = gr.cube('rthigh')
rthigh:scale(0.7, 1.2, 0.7)
rthigh:translate(0.0, -0.6, 0.0)
rthigh:set_material(blue)
rhip:add_child(rthigh)

rknee = gr.joint('rknee', {0,0,90}, {0,0,0})
rknee:translate(0.0, -1.1, 0.0)
rhip:add_child(rknee)

rcalf = gr.cube('rcalf')
rcalf:scale(0.6, 1.2, 0.6)
rcalf:translate(0.0, -0.6, 0.0)
rcalf:set_material(blue)
rknee:add_child(rcalf)

rankle = gr.joint('rankle', {0,0,90}, {0,0,0})
rankle:translate(0.0, -1.2, 0.0)
rknee:add_child(rankle)

rfoot = gr.cube('rfoot')
rfoot:scale(0.5, 0.4, 0.7)
rfoot:translate(0.0, 0.0, 0.35)
rfoot:set_material(blue)
rankle:add_child(rfoot)

boxy = gr.sphere('boxy')
boxy:scale(0.2, 0.2, 0.2)
boxy:translate(0.0, 0.0, 0.75)
boxy:set_material(red)
rootnode:add_child(boxy)

return rootnode
