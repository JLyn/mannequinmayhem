#include "material.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>

extern bool g_shadow;

Material::~Material()
{
}

PhongMaterial::PhongMaterial(const Colour& kd, const Colour& ks, double shininess)
  : m_kd(kd), m_ks(ks), m_shininess(shininess)
{
}

PhongMaterial::~PhongMaterial()
{
}

void PhongMaterial::apply_gl() const
{
  glEnable(GL_COLOR_MATERIAL);
  if (g_shadow)
  {
    float diffuse[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diffuse);

    float specular[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    //glMaterialfv(GL_FRONT, GL_SPECULAR, m_ks);

    glMaterialf(GL_FRONT, GL_SHININESS, 1.0f);
  }
  else
  {
    float diffuse[4] = {m_kd.R(), m_kd.G(), m_kd.B(), 1.0};
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glColor4f(m_kd.R(), m_kd.G(), m_kd.B(), 1.0);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, diffuse);

    float specular[4] = {m_ks.R(), m_ks.G(), m_ks.B(), 1.0};
    //glMaterialfv(GL_FRONT, GL_SPECULAR, m_ks);

    glMaterialf(GL_FRONT, GL_SHININESS, m_shininess);
  }
}
