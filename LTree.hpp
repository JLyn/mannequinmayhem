#ifndef LTREE_HPP
#define LTREE_HPP

#include <vector>

class LTree
{
  public:
    LTree(char *grammar, int size);
    virtual ~LTree();

  char * expand(int iterations);

  std::vector<char> m_grammar;
  std::vector<char *> m_rules;
};

#endif // LTREE_HPP
